import axios from "axios";
import { API_URL } from "config/constants";

export const setBaseUrl = () => {
  axios.defaults.baseURL = API_URL;

  axios.defaults.headers.common["Authorization"] =
    "token 79cf879fdf47951b90e01ceb1e53638612cc2fa8";
};
