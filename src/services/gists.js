import axios from "axios";

export default {
  fetchGistByUser: username => {
    return axios({
      method: "get",
      url: `users/${username}/gists`
    });
  }
};
