import React from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import "./App.css";

import { HomePage, SearchPage } from "containers";
import { Header } from "components";
import { setBaseUrl } from "config/axios";

setBaseUrl();

function App() {
  return (
    <div className="App">
      <Header />
      <Router>
        <Switch>
          <Route path={"/"} exact component={HomePage} />
          <Route path={"/users/:username/gists"} component={SearchPage} />
          <Route path="*" component={HomePage} />
        </Switch>
      </Router>
    </div>
  );
}

export default App;
