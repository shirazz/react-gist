import React from "react";

import "./styles/header.css";

const Header = () => {
  return (
    <h1 className="brand">
      <a className="home-link" href="/" title="home">
        Github Gists
      </a>
    </h1>
  );
};

export default Header;
