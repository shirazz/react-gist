import React from "react";
import PropTypes from "prop-types";

import "./styles/loader.css";

const Loader = props => {
  const { size } = props;
  return <span className={`loader ${size}`}>Loading..</span>;
};

Loader.propTypes = {
  gist: PropTypes.string
};

Loader.defaultProps = {
  size: "normal"
};

export default Loader;
