import React from "react";
import { withRouter } from "react-router-dom";
import "./styles/search.css";

const SearchInput = props => {
  const { history, location, match, staticContext, value, ...rest } = props;

  const handleSubmit = e => {
    e.preventDefault();
    if (value) {
      history.push(`/users/${value}/gists`, {
        user: value
      });
    }
  };

  return (
    <div className="search-container">
      <form>
        <div>
          <input value={value} {...rest} />
          <button
            type="submit"
            className="btn"
            onClick={handleSubmit}
            disabled={!value}
          >
            Search
          </button>
        </div>
      </form>
    </div>
  );
};

export default withRouter(SearchInput);
