import React from "react";
import PropTypes from "prop-types";

const SearchInfo = props => {
  const { listCount, user } = props;
  return (
    <div className="search-info">
      {listCount} results <strong>{user ? `for ${user}` : ""}</strong>
    </div>
  );
};

SearchInfo.propTypes = {
  listCount: PropTypes.number.isRequired,
  user: PropTypes.string.isRequired
};

export default SearchInfo;
