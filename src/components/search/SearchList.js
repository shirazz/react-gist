import React from "react";
import PropTypes from "prop-types";
import { Tag, Fork } from "components";

const SearchList = props => {
  const { list } = props;
  return (
    <ul className="search-list">
      {list.map(item => (
        <li className="search-list__item" key={item.url}>
          <span>{item.description || "No Description"}</span>
          <Tag gist={item} />
          <Fork url={item.forks_url} />
        </li>
      ))}
    </ul>
  );
};

SearchList.propTypes = {
  list: PropTypes.array.isRequired
};

export default SearchList;
