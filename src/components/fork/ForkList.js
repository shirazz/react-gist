import React from "react";
import PropTypes from "prop-types";
import "./styles/fork.css";

const getLatestForks = (lists, count) => {
  return lists
    .sort((a, b) => {
      return (
        new Date(b.created_at).getTime() - new Date(a.created_at).getTime()
      );
    })
    .splice(0, count);
};

const ForkList = props => {
  const { list } = props;
  const listItemCount = 3;
  const forks = getLatestForks(list, listItemCount);

  if (!forks.length) {
    return null;
  }

  return (
    <React.Fragment>
      <h4 className="title title_four">Recent Forks</h4>
      <ul className="fork-list">
        {forks.map(item => (
          <li className="fork-list__item" key={item.url}>
            <img
              src={item.owner.avatar_url}
              alt={item.owner.login}
              className="avatar"
            />
            <div>{item.owner.login}</div>
            <div className="hide">Created: {item.created_at}</div>
            <a
              href={item.html_url}
              target="_blank"
              rel="noopener noreferrer"
              className="virtual-link"
            >
              {item.html_url}
            </a>
          </li>
        ))}
      </ul>
    </React.Fragment>
  );
};

ForkList.propTypes = {
  list: PropTypes.array.isRequired
};

export default ForkList;
