import React, { useState, useEffect } from "react";
import axios from "axios";
import { ForkList, Loader } from "components";

const Fork = props => {
  const { url } = props;
  const [forks, setForks] = useState([]);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    const fetchData = async () => {
      setLoading(true);
      try {
        const result = await axios(url);
        setForks(result.data);
        setLoading(false);
      } catch (err) {
        setForks([]);
        setLoading(false);
        console.error(err);
      }
    };
    fetchData();
  }, [url]);

  return (
    <React.Fragment>
      {loading ? <Loader size="small" /> : <ForkList list={forks} />}
    </React.Fragment>
  );
};

export default Fork;
