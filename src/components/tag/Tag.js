import React from "react";
import PropTypes from "prop-types";
import "./styles/tag.css";

const getAssociatedFileTypes = function(gist) {
  const files = Object.values(gist.files);
  const uniqueFiles = [];
  for (let file of files) {
    const language = file.language || "Text";
    if (!uniqueFiles.includes(language)) {
      uniqueFiles.push(language);
    }
  }
  return uniqueFiles;
};

const Tag = props => {
  const gistFiles = getAssociatedFileTypes(props.gist);
  return (
    <ul className="tags">
      {gistFiles.map((item, index) => (
        <li className="tags__item" key={index}>
          <span>{item}</span>
        </li>
      ))}
    </ul>
  );
};

Tag.propTypes = {
  gist: PropTypes.object.isRequired
};

export default Tag;
