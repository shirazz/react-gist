import React, { useState, useEffect } from "react";
import { withRouter } from "react-router-dom";
import { useDebounce } from "use-debounce";
import { SearchInput, SearchList, SearchInfo, Loader } from "components";
import gistService from "services/gists";
import "./styles/search.css";

const SearchPage = props => {
  const { history, match } = props;
  const [user, setUser] = useState(match.params.username);
  const [gists, setGists] = useState([]);
  const [loading, setLoading] = useState(false);
  const [debouncedText] = useDebounce(user, 500);

  const changeHandler = e => {
    const inputValue = e.target.value;

    setUser(inputValue);
    if (inputValue) {
      history.push(`/users/${inputValue}/gists`, {
        user: inputValue
      });
    }
  };

  useEffect(() => {
    const fetchData = async () => {
      setLoading(true);
      try {
        const result = await gistService.fetchGistByUser(debouncedText);
        setGists(result.data);
        setLoading(false);
      } catch (err) {
        setGists([]);
        setLoading(false);
        console.log(err);
      }
    };
    fetchData();
  }, [debouncedText]);

  return (
    <section className="search">
      <div className="search-block">
        <SearchInput
          type="text"
          value={user}
          onChange={changeHandler}
          placeholder="Enter github username"
        />
      </div>

      {loading ? (
        <Loader size="medium" />
      ) : (
        <React.Fragment>
          <SearchInfo listCount={gists.length} user={debouncedText} />
          <SearchList list={gists} />
        </React.Fragment>
      )}
    </section>
  );
};
export default withRouter(SearchPage);
