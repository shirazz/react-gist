import React, { useState } from "react";
import { SearchInput } from "components";
import "./styles/home.css";

const HomePage = () => {
  const [user, setUser] = useState("");
  const changeHandler = e => {
    setUser(e.target.value);
  };

  return (
    <main className="main" role="main">
      <section className="section section__home-search">
        <div className="hero">
          <h3 className="hero__headline hero__headline--basic">
            Instantly search code, notes, and snippets from Github.
          </h3>
        </div>
        <section>
          <SearchInput
            type="text"
            value={user}
            onChange={changeHandler}
            placeholder="Enter github username"
          />
        </section>
      </section>
    </main>
  );
};

export default HomePage;
